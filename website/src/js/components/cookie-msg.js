var cookieMsg = {
    init: function(){
        var cookieStatementAccepted = localStorage.getItem("cookieStatementAccepted");
        if (!cookieStatementAccepted) {
             $(".cookie-msg").css('display','flex');

             $('.acceptCookieStatement').click(function(){
                 localStorage.setItem("cookieStatementAccepted", true);
                 $(".cookie-msg").css('display','none');
             });
        }
    }
}
