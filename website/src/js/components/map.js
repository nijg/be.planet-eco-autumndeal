function initMap() {
    var locations = {
        ninove: {
            coordinates: {lat: 50.841878,lng: 4.040115},
            dev: .3,
            stddev: .06,
            amount: 30
        },
        zottegem: {
            coordinates: {lat: 50.864810,lng: 3.823925},
            dev: .14,
            stddev: .06,
            amount: 15

        },
        gooik: {
            coordinates: {lat: 50.791137,lng: 4.131093},
            dev: .1,
            stddev: .08,
            amount: 15
        }
    },

    randomGeneratedLocations = [{lat: -25.363, lng: 131.044}];
    //marker = "/assets/img/marker.svg";

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: locations.ninove.coordinates,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
    });

    /*** Markers ***/
    for(var i = 0, j = randomGeneratedLocations.length; i< j; i++){
        var coordinates = randomGeneratedLocations[i];
        new google.maps.Marker({
            position: { lat: coordinates.lat, lng: coordinates.lng },
            //icon: marker,
            map: map
        });
    }


    //generateRandomLocations(locations);

}
/*
function generateRandomLocations(locations){
    var arr = [];
    Object.keys(locations).forEach(function(key){
        var location = locations[key];
        arr.push({lat:location.coordinates.lat,lng:location.coordinates.lng});
        for(var i = 0; i< location.amount; i++){
            arr.push({lat:location.coordinates.lat + randomProbability(location.dev,0,location.stddev),lng: location.coordinates.lng + randomProbability(location.dev,0,location.stddev)})
        }
    });
    console.log(JSON.stringify(arr));
}

function randomProbability(dev, target, stddev){
    var range = [-dev, dev];
    var takeGauss = (Math.random() < 0.81546);
    if(takeGauss) {
      // perform gaussian sampling (normRand has mean 0), resample if outside range
      while(1) {
        var sample = ((normRand()*stddev) + target);
        if(sample >= range[0] && sample <= range[1]) {
          return sample;
        }
      }
    } else {
      // perform flat sampling
      return range[0]+(Math.random()*(range[1]-range[0]));
    }
}

function normRand() {
    var x1, x2, rad;

    do {
        x1 = 2 * Math.random() - 1;
        x2 = 2 * Math.random() - 1;
        rad = x1 * x1 + x2 * x2;
    } while(rad >= 1 || rad == 0);

    var c = Math.sqrt(-2 * Math.log(rad) / rad);

    return x1 * c;
};
*/
