window.sr = ScrollReveal();

// Reveal sections
sr.reveal('section', {scale: 1});

// Reveal portfolio items
sr.reveal('.portfolio-item', { duration: 2000, scale: 1 }, 50);
