var equalHeight = {
    setEqualHeightByClassName: function(className) {
        var largest = 0;
        $(document).find(className).each(function() {
            var findHeight = $(this).height();
            if (findHeight > largest) {
                largest = findHeight;
            }
        });
        $(document).find(className).css({"height": largest + "px"});
    }
}
