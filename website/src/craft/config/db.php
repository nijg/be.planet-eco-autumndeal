<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
	'*' => array(
	    'driver' => getenv('DB_DRIVER'),
	    'server' => getenv('DB_SERVER'),
	    'user' => getenv('DB_USER'),
	    'password' => getenv('DB_PASSWORD'),
	    'database' => getenv('DB_DATABASE'),
	    'schema' => getenv('DB_SCHEMA'),
	    'tablePrefix' => getenv('DB_TABLE_PREFIX'),
	    'port' => getenv('DB_PORT')
	)
);
/*

return array(

	'*' => array(
		'tablePrefix' => 'craft',
	       'driver' => getenv('DB_DRIVER'),
	       'server' => getenv('DB_SERVER'),
	       'user' => getenv('DB_USER'),
	       'password' => getenv('DB_PASSWORD'),
	       'database' => getenv('DB_DATABASE'),
	       'schema' => getenv('DB_SCHEMA'),
	       'tablePrefix' => getenv('DB_TABLE_PREFIX'),
	       'port' => getenv('DB_PORT')
	),
	'planet-eco-autumndeal.local' => array(
		'server' => 'wildlymild.com',
		'database' => 'w15525wi_planet_eco_autumndeals',
		'user' => 'w15525wi_staging',
		'password' => 'mci7nwW3TVNu'
	),
	'planet-eco-autumndeal.staging.wildlymild.com' => array(
		'server' => 'wildlymild.com',
		'database' => 'w15525wi_planet_eco_autumndeals',
		'user' => 'w15525wi_staging',
		'password' => 'mci7nwW3TVNu'
	),
	'planet-eco-autumndeal.be' => array(
		'server' => 'planet-eco-autumndeal.be',
		'database' => 'planetau_oautumndeal',
		'user' => 'oautumn',
		'password' => 'zkoOYornM2oD'
	)

);//*/
