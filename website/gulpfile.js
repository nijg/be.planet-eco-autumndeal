/*
 * Gulp tasks:
 * default -> mamp-start + browser-sync
 * mamp-start (start mamp localhost server)
 * mamp-stop (stop mamp localhost server)
 * browser-sync (sync browser)
 * watch (watch src files + copies files to dist)
 * publish --staging (uploads dist to staging server)
 * publish --production (uploads dist to production server)
 */

var gulp = require('gulp'),
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    //notifier = require('node-notifier'),
    //css
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cleancss = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    //javascript
    concat = require('gulp-concat'),
    jsObfuscator = require('gulp-javascript-obfuscator'),
    babel = require('gulp-babel'),
    //uglify = require('gulp-uglify'),
    //browser
    mamp = require('gulp-mamp'),
    browserSync = require('browser-sync'),
    //filesystem
    fs = require('fs-extra'),
    del = require('del');

var config = JSON.parse(fs.readFileSync("./gulp-config.json"));

// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Gulp Default Task
// +++++++++++++++++++++++++++++++++++++++++++++++ //
gulp.task('default', ['mamp-start', 'browser-sync']);

// +++++++++++++++++++++++++++++++++++++++++++++++ //
// MAMP Tasks
// +++++++++++++++++++++++++++++++++++++++++++++++ //
var mampOptions = {};

gulp.task('mamp-start', function(cb) {
    mamp(mampOptions, 'start', cb);
});

gulp.task('mamp-stop', function(cb) {
    mamp(mampOptions, 'stop', cb);
});

// +++++++++++++++++++++++++++++++++++++++++++++++ //
// BrowserSync Task (Live reload)
// +++++++++++++++++++++++++++++++++++++++++++++++ //
gulp.task('browser-sync', function() {
    browserSync.init({
        open: 'external',
        host: config.dev.url,
        proxy: config.dev.url,
        port: 8080,
        browser: "google chrome"
    });
    gulp.watch('dist/**/*').on('change', function(e) {
        //browserSync.reload();
        //browserSync.stream({once: true});
        //notifier.notify({title: "BrowserSync reloaded", message: "We've updated the browser for you :-)"});
    });
});

// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Watch Task
// +++++++++++++++++++++++++++++++++++++++++++++++ //
gulp.task('watch', function() {

    gulp.watch('src/scss/**/*', ['sass']);
    gulp.watch('src/js/**/*', ['js-min']);

    gulp.watch([
        'src/public_html/**/*',
        'src/craft/**/*'
    ]).on('change', function(file) {
        //console.log("kaka");
        //console.log(JSON.stringify(file, null, 4));

        file.name = file.path.replace(/^.*[\\\/]/, '');
        file.destination = file.path.replace(__dirname + '/src/', 'dist/').replace(file.name, '');

        //console.log(JSON.stringify(file, null, 4));

        if (file.type === 'deleted') {
            del.sync(file.destination + file.name);
            //notifier.notify({title: file.name + " has been " + file.type , message: "File removed in " + file.destination});
            return;
        }

        return gulp.src(file.path)
            .pipe(gulp.dest(file.destination))
            .pipe(notify({
                title: file.name + " has been " + file.type,
                message: "File copied to " + file.destination
            }));
    });
});

// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Task to compile SCSS
// +++++++++++++++++++++++++++++++++++++++++++++++ //
gulp.task('sass', function() {
    return gulp.src('./src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
                errLogToConsole: false,
                outputStyle: 'expanded'
            })
            .on("error", notify.onError(function(error) {
                return "Failed to Compile SCSS: " + error.message;
            })))
        .pipe(autoprefixer())
        .pipe(rename('style.css'))
        //.pipe(gulp.dest('./dist/public_html/assets/css/'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cleancss({
            level: {
                1: {
                    specialComments: 1
                }
            }
        }))
        .pipe(sourcemaps.write('./sourcemap'))
        .pipe(gulp.dest('./dist/public_html/assets/css/'))
        /*.pipe(browserSync.reload({
            stream: true
        }))*/
        .pipe(notify("SCSS Compiled Successfully :)"));
});

// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Task to Concat & Minify JS
// +++++++++++++++++++++++++++++++++++++++++++++++ //
gulp.task('js-min', function() {
    return gulp.src([
            './src/js/vendor/bootstrap/util.js',
            './src/js/vendor/bootstrap/collapse.js',
            './src/js/vendor/scrollreveal.min.js',
            './src/js/components/**/*.js',
            './src/js/main.js',
        ])
        .pipe(concat('script.js'))
        .pipe(babel({
            presets: ['env']
        }))
        /*.pipe(jsObfuscator({
                compact: true,
                sourceMap: true
            })
            .on("error", notify.onError(function(error) {
                return "Failed to minify js: " + error.message;
            })))*/
        //.pipe(rename('script.min.js'))
        .pipe(gulp.dest('./dist/public_html/assets/js/'))
        .pipe(notify("JS Compiled Successfully :)"));
});

// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Minify Images
// +++++++++++++++++++++++++++++++++++++++++++++++ //
gulp.task('move-image-min', function() {
    return gulp.src('./src/img/**/*.+(png|jpg|jpeg|gif|svg)')
        // Caching images that ran through imagemin
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest('./dist/img'));

    gulp.src('./src/img/**/*.+(ico)')
        .pipe(gulp.dest('./dist/img'));;
});


// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Gulp Inline Source Task
// Embed scripts, CSS or images inline (make sure to add an inline attribute to the linked files)
// Eg: <script src="js-inline/default.js" inline></script>
// Will compile all inline within the html file (less http requests - woot!)
// +++++++++++++++++++++++++++++++++++++++++++++++ //
gulp.task('inline-source', function() {
    gulp.src('./src/**/*.html')
        .pipe(inlinesource())
        .pipe(gulp.dest('./dist/'));
});


// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Exit statements
// +++++++++++++++++++++++++++++++++++++++++++++++ //
// Actions on exit (e.g. finishing gulp task)
/*process.on('exit', function() {
    gulp.start('mamp-stop');
    console.log('Goodbye!');
});*/
//catches ctrl+c event and evokes exit
//process.on('SIGINT', process.exit);
//catches "kill pid" event and evokes exit
//process.on('SIGUSR1', process.exit);
//process.on('SIGUSR2', process.exit);
//catches uncaught exceptions and evokes exit
//process.on('SIGUSR1', process.exit);
