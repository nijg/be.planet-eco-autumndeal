<?php

return [
    //model
    "To Email" => "E-mail bestemmeling",
    'Your Name' => "Uw naam",
    'Your Email' => "Uw e-mailadres",
    'Message' => "Bericht",
    'Subject' => "Onderwerp",

    //plugin settings
    "The email address(es) that the contact form will send to. Separate multiple email addresses with commas."
        => "De e-mailadres(sen) waar dit contactform naartoe stuurt. Scheid verschillende e-mailadressen met een komma.",
    "Sender Text"
        => "Tekst afzender",
    "Text that will be prepended to the email's From Name to inform who the Contact Form actually was sent by."
        => "Tekst dat voorafgaat aan de ontvanger's naam, om aan te duiden wie het contactformulier verstuurde.",
    "On behalf of"
        => "In naam van",
    "Subject Text"
        => "Onderwerp tekst",
    "Text that will be prepended to the email's Subject to flag that it comes from the Contact Form."
        => "Tekst voorafgaand aan het onderwerp van deze e-mail, om aan aan te duiden dat het afkomstig is van het contactformulier.",
    "Allow attachments?"
        => "Bijlagen toestaan?",
    "Success Flash Message"
        => "Succes bericht",
    "The flash message diplayed after successfully sending a message."
        => "Het bericht dat wordt getoond nadat een boodschap met succes werd verzonden.",
    "Your message has been sent."
        => "Uw boodschap werd verzonden.",
    "There was a problem with your submission, please check the form and try again!"
        => "Je bericht kon niet worden verzonden. Kijk het formulier na en probeer opnieuw!"
];
