# planet-eco-autumndeal.be

## Twoons

**Twoons** is the project owner for **planet-eco-autumndeal.be**

### Contact Details

 **Twoons Comm. V.**  
Akrenbos 68C  
1547 Bever  
België  

**VAT** BE 0633 892 624

**Client id** 36  

### People

**Chris Decroix**  
(primary contact)  
**T** +32 472 61 13 54  
**T** +32 472 61 13 54  
**E** chris@twoons.be  
